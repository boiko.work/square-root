# Square Root

![Thunderbird screenshot](assets/thunderbird-screenshot.png "Thunderbird screenshot")

This is a flat and square theme for Thunderbird Daily v119.0a1

![Desktop screenshot](assets/desktop-screenshot.png "Desktop screenshot")

To use the theme:
1. Change into Thunderbird's profile directory (replace path with your actual profile directory)

    ```cd ~/.thunderbird/xxxxxxxx.default-nightly```
2. Clone the repository:

    ```git clone git@gitlab.com:boiko.work/square-root.git chrome```
